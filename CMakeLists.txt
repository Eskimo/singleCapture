cmake_minimum_required(VERSION 2.6)
project(singlecapture)

set( CMAKE_CXX_COMPILER "g++" )
set( CMAKE_BUILD_TYPE "Release" )
set( CMAKE_CXX_FLAGS "-std=c++11 -march=native -O3" )

find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_DIRS} )

add_executable(singlecapture main.cpp)
target_link_libraries( singlecapture ${OpenCV_LIBS} )
